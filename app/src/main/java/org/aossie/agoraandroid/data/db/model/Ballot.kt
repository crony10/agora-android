package org.aossie.agoraandroid.data.db.model

data class Ballot (
  var voteBallot: String ?= null,
  var hash:String ?= null
)